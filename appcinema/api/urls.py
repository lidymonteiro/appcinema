from django.contrib import admin
from appcinema.core.views import MovieListViewSet, ExhibitionListViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'cinema-filmes', MovieListViewSet)
router.register(r'cinema-exibicao', ExhibitionListViewSet)
urlpatterns = router.urls