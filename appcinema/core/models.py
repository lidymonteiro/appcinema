from django.db import models
from django_countries.fields import CountryField

class Movie(models.Model):

    DOCUMENTARY = 'DOC'
    DRAMA = 'DRA'
    FICTION = 'FIC'
    ANIMATION = 'ANI'
    
    GENRES = (
        (DOCUMENTARY, 'Documentário'),
        (DRAMA, 'Drama'),
        (FICTION, 'Ficção'),
        (ANIMATION, 'Animação')
    )

    title = models.CharField('título', max_length=250)
    slug = models.SlugField()
    synopsis = models.TextField('sinopse', max_length=500)
    year = models.IntegerField('ano')
    premiere = models.DateField('estréia', blank=True)
    distributor = models.CharField('distribuidora', max_length=300)
    rating = models.CharField('classificação', max_length=30, blank=True)
    free_rating = models.BooleanField('Livre')
    trailer = models.URLField('Trailer', help_text='Link do vídeo', blank=True)
    nationality = CountryField('Nacionalidade')
    runtime = models.IntegerField('duração', help_text='Em minutos', blank=True)
    genres =  models.CharField('gênero', max_length=3, choices=GENRES)
    image = models.ImageField('poster', upload_to='cinema/movies')
    directors = models.CharField('direção', max_length=500, blank=True)
    cast = models.CharField('elenco', max_length=500, blank=True)
    script = models.CharField('roteiro', max_length=200, blank=True)

    class Meta():
        verbose_name = 'filme'
        verbose_name_plural = 'filmes'

    def __str__(self):
        return self.title

    

class Exhibition(models.Model):

    movie = models.ForeignKey(Movie, verbose_name='Filme')
    start = models.DateField('Início')
    finish = models.DateField('Encerramento')

    class Meta:
        verbose_name = 'Em cartaz'
        verbose_name_plural = 'Em cartaz'

    def __str__(self):
        return self.movie.title

class Schedule(models.Model):
    CINE_FUNDAJ_DERBY = 'CFD'
    CINE_FUNDAJ_CASAFORTE = 'CFCF'
    
    CINEMA = (
        (CINE_FUNDAJ_CASAFORTE, 'Cinema da Fundação/Museu'),
        (CINE_FUNDAJ_DERBY, 'Cinema da Fundação/Derby')
    )

    data = models.DateTimeField('data')
    local = models.CharField('local', max_length=5, choices=CINEMA)
    #3d?
    exhibition = models.ForeignKey(Exhibition, verbose_name='Em cartaz')

    class Meta:
        verbose_name = 'programação'
        verbose_name_plural = 'programação'

    def __str__(self):
        return str(self.local)


