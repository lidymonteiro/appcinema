from django.shortcuts import render
from rest_framework import viewsets
from .serializer import MovieSerializer, ExhibitionSerializer
from .models import Movie, Schedule, Exhibition

def home(request):
    return render(request, 'index.html')

class MovieListViewSet(viewsets.ModelViewSet):
    serializer_class = MovieSerializer
    queryset = Movie.objects.all()

class ExhibitionListViewSet(viewsets.ModelViewSet):
    serializer_class = ExhibitionSerializer
    queryset = Exhibition.objects.all()