from django.contrib import admin
from appcinema.core.models import Movie, Schedule, Exhibition

class ScheduleInline(admin.TabularInline):
    model = Schedule
    extra = 1

class MovieModelAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title', )}
    list_display = ['title', 'genres', 'runtime', 'nationality', 'free_rating']
    fieldsets = ('Filme', {'fields': ['title', 'slug', 'synopsis', 'image', 'trailer', 
                                       'year', 'premiere', ('rating', 'free_rating'),
                                       'runtime', 'genres', 'nationality','distributor', 
                                       'directors', 'cast', 'script']}),

class ExhibitionModelAdmin(admin.ModelAdmin):
    inlines = [ScheduleInline]
    list_display = ['movie', 'start', 'finish']
    fieldsets = ('Em cartaz', {'fields': ['movie', ('start', 'finish')]}),

admin.site.register(Movie, MovieModelAdmin)
admin.site.register(Exhibition, ExhibitionModelAdmin)
