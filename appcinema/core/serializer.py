from rest_framework import serializers
from .models import Movie, Schedule, Exhibition


class MovieSerializer(serializers.ModelSerializer):

    class Meta:
        model = Movie
        fields = '__all__'
        depth = 1


class ScheduleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Schedule
        fields = ['id', 'data', 'local']
        depth = 2
       


class ExhibitionSerializer(serializers.ModelSerializer):
    movie = MovieSerializer(read_only=True)
    movieId = serializers.PrimaryKeyRelatedField(write_only=True, 
                                                 queryset=Movie.objects.all(),
                                                 source='movie')

    schedule = serializers.SerializerMethodField()

    class Meta:
        model = Exhibition
        fields = ['movie', 'movieId', 'start', 'finish', 'schedule']

    def get_schedule(self, obj):
        return [ScheduleSerializer(s).data for s in obj.schedule_set.all()]
